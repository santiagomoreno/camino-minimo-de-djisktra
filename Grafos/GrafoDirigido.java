package Grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class GrafoDirigido {
	// Representamos un grafo por listas de vecinos
	private ArrayList<HashSet<Integer>> _vecinos;
	
	// Un grafo se construye con todos sus v�rtices aislados
	public GrafoDirigido(int vertices){
		_vecinos = new ArrayList<HashSet<Integer>>(vertices);
		for(int i=0; i<vertices; ++i)
			_vecinos.add(new HashSet<Integer>());
	}
	
	// Agregado de una arista
	protected void agregarArista(int i, int j)
	{
		chequearIndices(i, j);
		chequearQueNoExista(i, j);

		_vecinos.get(i).add(j);
	}
	
	// Borrado de una arista
	 public void borrarArista(int i, int j)
	{
		chequearIndices(i, j);
		chequearQueExista(i, j);
		
		_vecinos.get(i).remove(j);
	}

	// Informa si existe la arista (i,j)
	 public boolean existeArista(int i, int j)
	{
		chequearIndices(i, j);
		return _vecinos.get(i).contains(j);
	}
	
	// Vecinos de un v�rtice
	 public Set<Integer> vecinos(int i)
	{
		chequearVertice(i);
		return _vecinos.get(i); // :)
	}
	
	// Cantidad de v�rtices y aristas
	 public int vertices()
	{
		return _vecinos.size();
	}
	 protected int aristas()
	{
		int ret = 0;
		for(int i=0; i<vertices(); ++i)
		for(int j=i+1; j<vertices(); ++j) if( existeArista(i,j) )
			++ret;
		
		return ret;
	}
	
	// Chequea que (i,j) corresponda a una arista posible
	private void chequearIndices(int i, int j)
	{
		if( i < 0 || i >= vertices() )
			throw new IllegalArgumentException("Se intenta procesar una "
					+ "arista con un vertice fuera de rango! i = " + i);

		if( j < 0 || j >= vertices() )
			throw new IllegalArgumentException("Se intenta procesar una "
					+ "arista con un vertice fuera de rango! j = " + j);
		
		if( i == j )
			throw new IllegalArgumentException("Se intenta procesar una "
					+ "arista entre un vertice y el mismo! i = j = " + i);
	}

	// Protesta si la arista (i,j) existe
	private void chequearQueNoExista(int i, int j)
	{
		if( existeArista(i, j) )
			throw new IllegalArgumentException("La arista (" + i + ", " + j
					+ ") ya existe!");
	}
	
	// Protesta si la arista (i,j) no existe
	protected void chequearQueExista(int i, int j)
	{
		if( !existeArista(i, j) )
			throw new IllegalArgumentException("La arista (" + i + ", " + j
					+ ") no existe!");
	}

	// Protesta si el vertice i no existe
	private void chequearVertice(int i)
	{
		if( i < 0 || i >= vertices())
			throw new IllegalArgumentException("Se consulta un "
				+ "vertice inexistente! i = " + i);
	}
}
