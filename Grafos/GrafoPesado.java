package Grafos;


public class GrafoPesado extends GrafoDirigido{
	// Matriz de pesos
	private int pesos[][];
	
	public GrafoPesado(int vertices){
		super(vertices);
		setPesos(new int[vertices][vertices]);
	}	
	
	public int peso(int i, int j){
		if(getPesos()[i][j]==0)return 0;
		return getPesos()[i][j];
	}
	 public void agregarArista(int i, int j, int peso){
		agregarArista(i, j);

		getPesos()[i][j] = peso;
	
	}

	public void imprimir() {
		for(int i=0;i<getPesos().length;i++){
			for(int j=0;j<getPesos()[i].length;j++){
				System.out.println(getPesos()[i][j]+"epos");
			}}
		
		
		

		
		
	}

	public int[][] getPesos() {
		return pesos;
	}

	public void setPesos(int pesos[][]) {
		this.pesos = pesos;
	}
	
	

}

