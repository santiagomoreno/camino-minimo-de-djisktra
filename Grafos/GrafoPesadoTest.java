package Grafos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GrafoPesadoTest
{
	private GrafoPesado _grafo;
	
	@Before
	public void inicializar()
	{
		_grafo = new GrafoPesado(4);
		_grafo.agregarArista(0, 1, 10);
		_grafo.agregarArista(1, 2, 20);
	}
	
	@Test
	public void consultarPeso()
	{
		assertEquals(20, _grafo.peso(1, 2), 1e-5);
	}


	@Test
	public void agregarAristaSinPeso()
	{
		_grafo.agregarArista(1, 3);
		
		assertTrue(_grafo.existeArista(1, 3));
		assertEquals(0, _grafo.peso(1, 3), 1e-5);
	}
}









