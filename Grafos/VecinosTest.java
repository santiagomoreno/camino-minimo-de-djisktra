package Grafos;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

public class VecinosTest
{
	@Test(expected = IllegalArgumentException.class)
	public void verticeNegativoTest()
	{
		GrafoDirigido grafo = ejemplo();
		grafo.vecinos(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticeExcedidoTest()
	{
		GrafoDirigido grafo = ejemplo();
		grafo.vecinos(6);
	}

	@Test
	public void tresVecinosTest()
	{
		GrafoDirigido grafo = ejemplo();
		Set<Integer> vecinos = grafo.vecinos(3);
		assertEquals(false,sonIguales(new int[] {0, 1, 2}, vecinos));
	}

	private boolean sonIguales(int[] is, Set<Integer> vecinos) {
		if(vecinos.size()==is.length){
			for(int i=0;i<is.length;i++){
				if(!vecinos.contains(is[i]))
					return false;
			}
			return true;
		}
		return false;
	}

	@Test
	public void unVecinoTest()
	{
		GrafoDirigido grafo = ejemplo();
		Set<Integer> vecinos = grafo.vecinos(0);
		assertEquals(true,sonIguales(new int[] {2,3,4}, vecinos));	}

	@Test
	public void sinVecinosTest()
	{
		GrafoDirigido grafo = ejemplo();
		Set<Integer> vecinos = grafo.vecinos(5);
		assertEquals(true,sonIguales(new int[] {}, vecinos));	}

	private GrafoDirigido ejemplo()
	{
		GrafoDirigido grafo = new GrafoDirigido(6);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(0, 4);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 3);
		return grafo;
	}
}










