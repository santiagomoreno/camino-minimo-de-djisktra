package Grafos;

import static org.junit.Assert.*;

import org.junit.Test;

public class AristasTest
{
	@Test(expected = IllegalArgumentException.class)
	public void verticeNegativoTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(-1, 2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void verticeExcedidoTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(5, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeNegativoTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(2, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void segundoVerticeExcedidoTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(2, 5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void verticesIgualesTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(2, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void aristaExistenteTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 3);
	}
	
	@Test
	public void agregarAristaTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(2, 3);
		assertTrue( grafo.existeArista(2, 3));
	}

	

	@Test
	public void aristaInexistenteTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(1, 3);
		assertFalse( grafo.existeArista(3, 2));
	}

	@Test
	public void verticesInicianEnCeroTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(0, 3);
		assertFalse( grafo.existeArista(3, 0));
	}

	@Test
	public void verticeMaximoTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista( 4,0);
		assertTrue( grafo.existeArista(4, 0));
	}

	@Test
	public void borrarAristaExistenteTest()
	{
		// Setup
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(0, 4);
		
		// Exercise
		grafo.borrarArista(0,4);
		
		// Verify
		assertFalse( grafo.existeArista(4, 0));
	}
	
	@Test
	public void verticesTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		assertEquals(5, grafo.vertices());
	}
	
	@Test
	public void verticesConAristasTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(1, 4);
		assertEquals(5, grafo.vertices());
	}
	
	@Test
	public void aristasVaciasTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		assertEquals(0, grafo.aristas());
	}
	
	@Test
	public void aristasNoVaciasTest()
	{
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(1, 4);
		assertEquals(2, grafo.aristas());
	}
}




