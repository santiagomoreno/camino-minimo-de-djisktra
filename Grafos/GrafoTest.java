package Grafos;

import static org.junit.Assert.*;



import org.junit.Test;

public class GrafoTest {

	@Test
	public void Aristastest() {
		GrafoDirigido nuevo=new GrafoDirigido(5);
		nuevo.agregarArista(0, 3);
		nuevo.agregarArista(0, 2);
		nuevo.agregarArista(0, 1);
		assertEquals(3,nuevo.aristas());

	}
	@Test(expected=IllegalArgumentException.class)
	public void aristaRepetida(){
		GrafoDirigido nuevo=new GrafoDirigido(5);
		nuevo.agregarArista(0, 3);
		nuevo.agregarArista(0, 3);
		assertEquals(1,nuevo.aristas());

	}
	


}
