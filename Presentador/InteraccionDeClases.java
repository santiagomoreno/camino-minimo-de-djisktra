package Presentador;

import java.util.ArrayList;

import Mapa.MapaConMinas;

public class InteraccionDeClases {
	private MapaConMinas mapa;
	
	public InteraccionDeClases(){
		setMapa(new MapaConMinas());
		
	}
	public void agregarPunto(){
		getMapa().agregarPuntoInteres();
	}
	public void agregarCarbonApunto(int x, int carbon){
		getMapa().agregarCarbonMina(carbon, x);
	}
	public void agregarTunel(int x, int y, int peso){
		getMapa().agregarTunel(x, y, peso);
	}
	public ArrayList<Integer> recorridoMinimo(int x){
		return getMapa().recorridoMinimo(x);
	}
	public MapaConMinas getMapa() {
		return mapa;
	}
	public void setMapa(MapaConMinas mapa) {
		this.mapa = mapa;
	}
}
