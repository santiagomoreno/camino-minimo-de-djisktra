package Mapa;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import Grafos.GrafoPesado;

public class DijkstraTest {

	@Test
	public void caminoMinSimpletest() {
		GrafoPesado nuevo=new GrafoPesado(5);
		nuevo.agregarArista(0, 2, 50);
		nuevo.agregarArista(0, 1, 60);
	
		Dijkstra viejo=new Dijkstra(nuevo);		
		ArrayList<Integer>lista=new ArrayList<>();
		lista.add(0);
		lista.add(1);
		assertTrue(sonIguales(lista,viejo.caminoMinimo(0, 1)));

	}
	@Test
	public void caminoMinComplejotest() {
		GrafoPesado nuevo=new GrafoPesado(5);
		nuevo.agregarArista(0, 2, 50);
		nuevo.agregarArista(0, 1, 200);
		nuevo.agregarArista(1, 3, 60);
		nuevo.agregarArista(1, 2, 10);
		nuevo.agregarArista(2, 4, 14);
		nuevo.agregarArista(2, 3, 42);
		nuevo.agregarArista(3, 4, 20);
		
		
		Dijkstra viejo=new Dijkstra(nuevo);		
		ArrayList<Integer>lista=new ArrayList<>();
		lista.add(0);
		lista.add(2);
		lista.add(4);
	
		assertTrue(sonIguales(lista,viejo.caminoMinimo(0, 4)));
	}
	@Test
	public void caminoMinConIgualdadIzqDertest() {
		GrafoPesado nuevo=new GrafoPesado(5);
		nuevo.agregarArista(0, 2, 50);
		nuevo.agregarArista(0, 1, 50);
		nuevo.agregarArista(1, 3, 60);
		nuevo.agregarArista(1, 2, 10);
		nuevo.agregarArista(2, 4, 14);
		nuevo.agregarArista(2, 3, 42);
		nuevo.agregarArista(3, 4, 20);
		//se queda con la opcion mas alta de numero
		
		Dijkstra viejo=new Dijkstra(nuevo);		
		ArrayList<Integer>lista=new ArrayList<>();
		lista.add(0);
		lista.add(2);
		lista.add(4);
	
		assertTrue(sonIguales(lista,viejo.caminoMinimo(0, 4)));
	}
	@Test(expected=IllegalArgumentException.class)
	public void caminoMinVaciotest() {
		GrafoPesado nuevo=new GrafoPesado(0);
	
		Dijkstra viejo=new Dijkstra(nuevo);		
		
		viejo.caminoMinimo(0, 1);
		
	}
	@Test
	public void caminoMinIgualtest() {
		GrafoPesado nuevo=new GrafoPesado(2);
	
		Dijkstra viejo=new Dijkstra(nuevo);		
		
		ArrayList<Integer>lista=new ArrayList<>();
		lista.add(1);
		
		assertEquals(lista,viejo.caminoMinimo(1, 1));
		
	}
	@Test(expected=IllegalArgumentException.class)
	public void caminoMinNegativotest() {
		GrafoPesado nuevo=new GrafoPesado(2);
	
		Dijkstra viejo=new Dijkstra(nuevo);		
		
		viejo.caminoMinimo(1, -1);
		
	}
	@Ignore
	@Test
	public void caminoInalcantest() {
		GrafoPesado nuevo=new GrafoPesado(5);
		nuevo.agregarArista(0, 2, 50);
		nuevo.agregarArista(0, 1, 60);
		Dijkstra viejo=new Dijkstra(nuevo);
		viejo.caminoMinimo(0, 4);
		
	}



	private boolean sonIguales(ArrayList<Integer> lista,
			ArrayList<Integer> caminoMinimo) {
		for(int i=0;i<lista.size();i++){
			if(!lista.get(i).equals(caminoMinimo.get(i))){
				return false;
			}
		}return true;
	}

}
