package Mapa;

import static org.junit.Assert.*;

import org.junit.Test;

import Grafos.GrafoPesado;

public class MapaConMinasTest {

	@Test
	public void cantPuntosInterestest() {
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		assertEquals(4,nuevo.cantPuntos());	
	}
	@Test
	public void cantPuntosInteresRecienCreadotest() {
		MapaConMinas nuevo= new MapaConMinas();
		assertEquals(nuevo.cantPuntos(),0);
	}
	@Test
	public void PosicionMinaUnitariatest() {
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarCarbonMina(50, 0);
		assertEquals(50,nuevo.cantCarbonEn(0));
	}
	@Test
	public void PosicionMinaGenericatest() {
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres( );
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarCarbonMina(440, 2);
		nuevo.agregarCarbonMina(50, 3);

		assertEquals(440,nuevo.cantCarbonEn(2));
	}
	@Test(expected=IllegalArgumentException.class)
	public void PosicionMinaIncorrectotest() {
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarCarbonMina(50,8);
		assertEquals(50,nuevo.cantCarbonEn(8));
	}
	@Test
	public void verCantTunelestest(){
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarTunel(0, 1, 50);
		assertEquals(true,nuevo.hayTunel(0, 1));
	}
	@Test(expected=IllegalArgumentException.class)
	public void CantTunelesIncorrectotest(){
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
	
		nuevo.agregarTunel(1, 1, 50);
		assertEquals(true,nuevo.hayTunel(0, 1));
	}
	@Test(expected=IllegalArgumentException.class)
	public void CantTunelesIncorrectoPesotest(){
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarTunel(0, 1, -50);
		assertEquals(true,nuevo.hayTunel(0, 1));
	}
	@Test
	public void CantTunelesMaltest(){
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarTunel(0,2, 50);
		assertEquals(true,nuevo.hayTunel(0, 2));
	}
	@Test
	public void posicionesPuntostest(){
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		
		assertEquals(0,nuevo.cantCarbonEn(2));
	}
	@Test
	public void copiarGrafotest(){
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		GrafoPesado viejo= new GrafoPesado(nuevo.cantPuntos()+1);
		nuevo.copiarGrafo(viejo);
		
		assertEquals(nuevo.cantPuntos(),4);
	}
	@Test
	public void copiarGrafBientest(){
		MapaConMinas nuevo= new MapaConMinas();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		nuevo.agregarPuntoInteres();
		
		assertEquals(nuevo.cantPuntos(),4);
	}
	
}
