/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Mapa;

import java.util.ArrayList;

import Grafos.GrafoPesado;


public class Dijkstra{
	private int distancia[];
	private int [] padre;
	private boolean[] visto;
	private GrafoPesado grafo;
	
	
	public Dijkstra(GrafoPesado g){
		grafo=g;
		
		distancia=new int[grafo.vertices()];
		padre= new int[grafo.vertices()];
		visto=new boolean[grafo.vertices()];
	}
	
	
	public void cambiarG(GrafoPesado otroG){
		grafo=otroG;
		distancia=new int[grafo.vertices()];
		padre= new int[grafo.vertices()];
		visto=new boolean[grafo.vertices()];
	}
	
	public ArrayList<Integer> caminoMinimo(int inicio, int fin){
		
		verificarValores(inicio,fin);

		int[]dij=caminoDijkstra(inicio,fin);
		
		ArrayList<Integer>resp=new ArrayList<>();
		int aux=fin;
		
		while(aux!=inicio){
			resp.add(aux);

			aux=dij[aux];
		}
		dij= new int[0];
		resp.add(inicio);
		return invertir(resp);
	}
	
	private ArrayList<Integer> invertir(ArrayList<Integer> resp) {
		ArrayList<Integer>act=new ArrayList<>();
		for(int i=resp.size()-1;i>=0;i--){
			act.add(resp.get(i));
		}
		return act;
	}
	private int[] caminoDijkstra(int inicio,int fin){
		
		int[]anteriores=new int[grafo.vertices()];
		
		for(int i=0;i<grafo.vertices();i++){
			distancia[i]=Integer.MAX_VALUE;
			padre[i]=-1;
			anteriores[i]=-1;
		}
		distancia[inicio]=0;
		
		int contador=1;
		while(!visto[fin] || contador==1){
			contador--;
			int u=inicio;
			int aux=Integer.MAX_VALUE;
			
			for(int i=0;i<visto.length;i++){
				if(visto[i]==false){
					if(aux>distancia[i]){
						aux=distancia[i];
						u=i;
					}
				}
			}
			visto[u] = true;
			
			for(Integer vecino : grafo.vecinos(u)){
				int distanciaAVecino = distancia[u] + grafo.peso(u, vecino);
				if(distanciaAVecino < distancia[vecino]){
					distancia[vecino] = distanciaAVecino;
					anteriores[vecino]=u;
				}
			}
		}
		int[]nuevo={-1,-1,-1};
		
		distancia=new int[grafo.vertices()];
		visto=new boolean[grafo.vertices()];
		if(contador==0)return nuevo;
		
		return anteriores;
	}
	private void verificarValores(int inicio, int fin) {
		
		if(inicio<0 || fin<0|| inicio>=grafo.vertices()|| fin>=grafo.vertices())
			throw new IllegalArgumentException("valores inicio/fin incorrectos");
	}
	
}
