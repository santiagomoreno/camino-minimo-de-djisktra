package Mapa;

import java.util.ArrayList;







import Grafos.GrafoPesado;

public class MapaConMinas {
public  GrafoPesado Mapa;
	private ArrayList<Integer> carbonEnPunto;
	private Dijkstra distancias;
	
	public MapaConMinas(){
		Mapa=new GrafoPesado(0);
		carbonEnPunto=new ArrayList<Integer>();
		setDistancias(new Dijkstra(Mapa));
	}
	public void agregarPuntoInteres(){
		GrafoPesado nuevo=new GrafoPesado(Mapa.vertices()+1);
		copiarGrafo(nuevo);
		
		carbonEnPunto.add(0);
		
		
	}
	
	public void agregarTunel(int x, int y, int peso){
		if(x<0 || y<0 || x>cantPuntos() || y>cantPuntos()||peso<0)
			throw new IllegalArgumentException("tunel o peso incorrecto");
			
		Mapa.agregarArista(x,y, peso);
	}
	
	public void agregarCarbonMina(int carbon,int x){
		if(x<0 || x>=carbonEnPunto.size())
			throw new IllegalArgumentException("valor de entrada incorrecto");
		carbonEnPunto.set(x,carbon);	
	}
	public ArrayList<Integer> recorridoMinimo(int a){
		
		ArrayList<ArrayList<Integer>> caminosACarbon=new ArrayList<>();
		ArrayList<Integer> carbones=nodosConCarbon();
		
		for(int i=0;i<carbones.size();i++){
			caminosACarbon.add(getDistancias().caminoMinimo(a,carbones.get(i)));
			
		}
		
		return rutaMasCorta(caminosACarbon);
	}
	
	int cantPuntos(){
		return Mapa.vertices();
	}
	protected int cantCarbonEn(int i){
		if(i<0 || i>=carbonEnPunto.size())
			throw new IllegalArgumentException("valor de entrada incorrecto");
		return carbonEnPunto.get(i);
	}
	protected boolean hayTunel(int i, int b){
		return Mapa.existeArista(i, b);
	}
	protected void copiarGrafo(GrafoPesado segundo){
		for(int i=0;i<Mapa.vertices();i++){
			ArrayList<Integer>vecinosDeI=vecinosDe(i);
			for(int j=0;j<vecinosDeI.size();j++){
				segundo.agregarArista(i, vecinosDeI.get(j), Mapa.peso(i, vecinosDeI.get(j)));
			}
		}
		Mapa=segundo;
	}
	private ArrayList<Integer> vecinosDe(int i) {
		ArrayList<Integer>vecinos=new ArrayList<Integer>();
		for(int j=0;j<Mapa.vertices();j++){
			if(Mapa.vecinos(i).contains(j))
				vecinos.add(j);
		}
		return vecinos;
	}
	
	
	private ArrayList<Integer> nodosConCarbon(){
		ArrayList<Integer> ret=new ArrayList<>();
		for(int i=0;i<carbonEnPunto.size();i++){
			if(carbonEnPunto.get(i)>0){
				ret.add(i);
			}
		}
		return ret;
	}
	
	
	
private int pesoRuta(ArrayList<Integer> ruta) {
		
		int peso=0;
		for(int i=0;i<ruta.size()-1;i++){
			peso=peso+Mapa.getPesos()[ruta.get(i)][ruta.get(i+1)];
		}
		return peso;
	}



private ArrayList<Integer> rutaMasCorta(ArrayList<ArrayList<Integer>> todasLasRutas){
	ArrayList<Integer> candidato=new ArrayList<>();
	int cant=Integer.MAX_VALUE ;
	for(ArrayList<Integer> list : todasLasRutas){
		if(cant>pesoRuta(list)){
			
			candidato=list;
			cant=pesoRuta(list);
		}
	}
						
	return candidato;
}
public Dijkstra getDistancias() {
	return distancias;
}
public void setDistancias(Dijkstra distancias) {
	this.distancias = distancias;
}
	
}
