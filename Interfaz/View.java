package Interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class View {

	private JFrame frame;
	
	private Principal principal = new Principal();;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View window = new View();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public View() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("CREADOR DE MAPA DE MINAS");
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		
		
		frame.getContentPane().add(principal);
		principal.setLayout(null);
		

		JOptionPane.showMessageDialog(null, "CLICK IZQUIERDO:crea puntos interes// CLICK DERECHO: crea tuneles", "FUNCIONALIDAD", JOptionPane.WARNING_MESSAGE);
		
		
		
		
		
	}
}
