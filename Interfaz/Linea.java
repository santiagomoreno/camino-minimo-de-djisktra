package Interfaz;

import java.awt.Color;
import java.awt.Graphics;

public class Linea{

    Circulo inicial;
    Circulo ffinal;
    Color color= Color.BLACK;
    String peso;

    public Linea(Circulo inicial, Circulo ffinal,String peso) {
        this.inicial=inicial;
        this.ffinal=ffinal;
        this.peso=peso;
    }

    public void painter(Graphics g) {

        
        g.drawLine(inicial.getX(),inicial.getY(), ffinal.getX(), ffinal.getY());
		if(inicial.getX()>ffinal.getX() && inicial.getY()>ffinal.getY())
			g.drawString(peso, inicial.getX()- Math.abs((inicial.getX()-ffinal.getX())/2), inicial.getY()- Math.abs((inicial.getY()-ffinal.getY())/2));
		if(inicial.getX()<ffinal.getX() && inicial.getY()<ffinal.getY())
			g.drawString(peso, ffinal.getX()- Math.abs((inicial.getX()-ffinal.getX())/2), ffinal.getY()- Math.abs((inicial.getY()-ffinal.getY())/2));
		if(inicial.getX()>ffinal.getX() && inicial.getY()<ffinal.getY())
			g.drawString(peso, inicial.getX()- Math.abs((inicial.getX()-ffinal.getX())/2), ffinal.getY()- Math.abs((inicial.getY()-ffinal.getY())/2));
		if(inicial.getX()<ffinal.getX() && inicial.getY()>ffinal.getY())
			g.drawString(peso, ffinal.getX()- Math.abs((inicial.getX()-ffinal.getX())/2), inicial.getY()- Math.abs((inicial.getY()-ffinal.getY())/2));
	}
        
 
        
        
    

    public void setColor(Color color) {
        this.color = color;
    }

    public Circulo getFfinal() {
        return ffinal;
    }

    public Circulo getInicial() {
        return inicial;
    }
    
}

