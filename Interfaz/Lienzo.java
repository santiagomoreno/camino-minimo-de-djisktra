package Interfaz;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Presentador.InteraccionDeClases;

//import Interfaz.PuntoInteres;

public class Lienzo extends JPanel implements MouseListener,MouseMotionListener{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int x=0,y=0;
	private Linea linea;
	private Circulo circulo;
	private Circulo jalada=null;
	private List<Circulo> ListCirculo = new ArrayList<Circulo>();
	private List <Linea> ListArista = new ArrayList<Linea>();
	private Principal p;
     InteraccionDeClases presentador;
    private int contNodos=0;
    

    public Lienzo(Principal prin) {
     p=prin;
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.setVisible(true);
        this.setDoubleBuffered(true);
        presentador=new InteraccionDeClases();
        
    }
   public int cantPunto(){
	   return ListCirculo.size();
   }

    public void anadirCirculo(int x, int y,String nom){
        circulo = new Circulo(ListCirculo.size(),x,y,nom);
        ListCirculo.add(circulo);
        repaint();
        p.repaint();
    }

    public void anadirLinea( int x, int y,String peso){
        try{
            linea = new Linea (ListCirculo.get(x),ListCirculo.get(y),peso);
            this.ListArista.add(linea);
            repaint();
            p.repaint();
        }catch(IndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(null, "No se encontro circulo");
        }
    }

    @Override
    public void paintComponent(Graphics g){
     super.paintComponents(g);
      for (Linea f:ListArista)
      {
       f.painter(g);
      }

      for (Circulo f:ListCirculo)
      {
       f.painter(g,this);
      }

    }

    public void mouseClicked(MouseEvent e) {
        try{
         if(e.getButton()==MouseEvent.BUTTON1){
        	 
        	 String nombre=""+contNodos;
        	 String carbon=JOptionPane.showInputDialog("tiene carbon: ");
             x = e.getX();
             y = e.getY();
            
             if(carbon!=null){
            	 anadirCirculo(x,y,nombre);
            	contNodos++;
 				presentador.agregarPunto();
 				
				
				presentador.agregarCarbonApunto(ListCirculo.size()-1, Integer.parseInt(carbon));
 				
			
 				repaint();
 				
 				
 				
 			}
             
             
             
         }else
         {
             if(e.getButton()==MouseEvent.BUTTON3)
             {
                int ini = Integer.parseInt(JOptionPane.showInputDialog("Numero de circulo inicual"));
                int fin = Integer.parseInt(JOptionPane.showInputDialog("Numero de circulo final"));
                String peso=JOptionPane.showInputDialog("ingrese distancia entre puntos: ");
                anadirLinea(ini, fin,peso);
                presentador.agregarTunel(ini,fin,Integer.parseInt(peso));

                                
                
             
                
             }
         }
        }catch(Exception ex){
            
        }
    }
    

    
    
    public ArrayList<Integer> cambiarPresentador(int x){
    	ArrayList<Integer> ret =new ArrayList<>();
    	
    	presentador.getMapa().getDistancias().cambiarG(presentador.getMapa().Mapa);
    	
    	ret= presentador.recorridoMinimo(x);
    	System.out.println("llega?"+ret);
    	return ret;
    	
    	
    }
    
    

    public void mousePressed(MouseEvent e) {}

    public void mouseReleased(MouseEvent e) {}

    public void mouseEntered(MouseEvent e) {}

    public void mouseExited(MouseEvent e) {}

    public void mouseDragged(MouseEvent e) {
       if(jalada==null)
        {
           for (Circulo f:ListCirculo)
           {
             if(f.jaladopor(e.getPoint()))
               {
                 jalada=f;
               }
             x=e.getPoint().x;
             y=e.getPoint().y;
             repaint();
             p.repaint();
            }
       }
       else{
           jalada.transladar(e.getPoint().x-x,e.getPoint().y-y);
           x=e.getPoint().x;
           y=e.getPoint().y;
           repaint();
           p.repaint();
           }
    }

    public void mouseMoved(MouseEvent e){
        jalada=null;
    }
    
    
    
    

}

