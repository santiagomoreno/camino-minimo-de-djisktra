package Interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class Principal extends JPanel {
	Lienzo l;
	private JLabel lblMostrarCamino;
	
	
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public Principal(){
  l=new Lienzo(this);
  l.setBounds(0, 0, 700, 400);
  add(l);
  
  lblMostrarCamino = new JLabel("Mostrar camino");
	lblMostrarCamino.setBounds(224, 442, 470, 14);
	add(lblMostrarCamino);
	lblMostrarCamino.setVisible(false);
  
  
  
  JButton btnEncontrarCaminoMinimo = new JButton("Encontrar camino minimo");
	btnEncontrarCaminoMinimo.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String aux=JOptionPane.showInputDialog("Numero de nodo inicual");
			 if(aux!=null && Integer.parseInt(aux)<l.cantPunto() ){
				 Integer ini = Integer.parseInt(aux);
      
         
          lblMostrarCamino.setVisible(true);
          lblMostrarCamino.setText("El camino es: "+encontrarCamino(ini));
          }
			
		}
	});
	btnEncontrarCaminoMinimo.setBounds(10, 438, 185, 23);
	add(btnEncontrarCaminoMinimo);
  
  
  
  
 }



public ArrayList<Integer> encontrarCamino(int x){
	ArrayList<Integer> ret=new ArrayList<>();
	
	ret =l.cambiarPresentador(x);
	return ret;
	
	
}




}

