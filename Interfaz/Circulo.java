package Interfaz;

import java.awt.*;

import javax.swing.JComponent;

@SuppressWarnings("serial")
public class Circulo extends JComponent{

    int x,y,n;
    Image image;
    String nombre;

    public Circulo(int n, int x, int y,String nombr) {
        this.n=n;
        this.x=x;
        this.y=y;

    	nombre=nombr;
    	
    }

    public void painter(Graphics g,Lienzo l) {

    	paintBorder(g);
		g.fillOval(x- 30/2, y-30/2, 30, 30);
		
		g.drawString(nombre, x+30, y);
        }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getN() {
        return n;
    }
    
    public void transladar(int dx,int dy) {
        this.x+=dx; this.y+=dy;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public boolean jaladopor(Point d) {
        if(d.distance(x, y)<=15) {
            return true;
        }
         else {
                return false;
         }
    }
}

